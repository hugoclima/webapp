'use strict';

angular.module('app', ['ui.router','app.controller','app.services'])

.config(function($stateProvider, $urlRouterProvider) {
  
  $urlRouterProvider.otherwise("/home");
  
    $stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "templates/aviso.html",
      controller: 'View1Ctrl'
    })
    .state('usuarios', {
      url: "/usuarios",
      templateUrl: "templates/gruds/grudUsuarios.html",
      controller: 'CrtlUsuarios'
    })
    .state('usuariosForms', {
      url: "/usuariosForms",
      templateUrl: "templates/forms/formUsuarios.html",
      controller: 'CrtlUsuarios'
    });
    
});