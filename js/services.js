
var baseUrl = 'http://10.0.1.22\\:8080';

angular.module('app.services', ['ngResource'])

.factory('UsersFactory', function ($resource) {
    return $resource(baseUrl + '/snmpweb/rest/usuarios/listar', {}, {
        query: { method: 'GET', isArray: true }       
    });
})

.factory('UserFactory', function ($resource) {
    return $resource(baseUrl + '/snmpweb/rest/usuarios/listar/:id', {}, {
        show: { method: 'GET' },
        update: { method: 'PUT', params: {id: '@id'} },
        delete: { method: 'DELETE', params: {id: '@id'} }
    });
})


.factory('UserServices', ['$resource',
	function($resource) {
		//return $resource(servidor+':3000'+'/lista/',{},/ Usar esta forma quando o codigo estiver fora do servidor
		return $resource(baseUrl + '/snmpweb/rest/usuarios/:id',{},
			{
				get: { method:'GET'},
				delete: { method:'DELETE'}
				
			});
	}
])

.factory('UserGet', ['$resource',
	function($resource) {
		return $resource(baseUrl + '/snmpweb/rest/usuarios/listar/',{},
			{
				query: { method:'GET', isArray:true },
				save: { method:'POST'}, 
                                update: { method:'PUT'}
                                
			});
	}
])

.factory('AuthenticationService', function() {
	var auth = {
		isAuthenticated: false,
		isAdmin: false,
		isID: false,
		isUpdate: false
	};
	return auth;
});
