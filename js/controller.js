'use strict';

angular.module('app.controller', [])

.controller('View1Ctrl', [function() {
        
}])

.controller('UsuariosCtrl', ['$scope', 'UsersFactory','$window','$state', function ($scope, UsersFactory,$window,$state) {
    
                
    
        console.log("Bem vindo ao Sistema!");
    
    
        
}])

.controller('CrtlUsuarios', ['$window','$state','$scope','$location', 'UserServices', 'UserGet',
    function($window, $state, $scope,  $location, UserServices, UserGet) {
        
        $scope.load = function() { 
            $window.localStorage.removeItem('idUsuarios');          
            $scope.dados = {};
            $scope.dado = {};
        };
        
        $scope.$on('recarregar', function (){
            $scope.dados = UserGet.query();
        });
        
        $scope.$watch('dado', function change(novoValor, antigoValor){
            $scope.$broadcast('recarregar');
        });
     
        //Pega Registro por ID e Envia por Form
        $scope.atualizar = function(userid) { 
            //$location.path("/usuariosForms").replace();
            $state.go("usuariosForms");
            $window.localStorage.setItem('idUsuarios', userid);
        };

        //Pega dos dados no Caso de Atualizar
        $scope.loadID = function() {
            var userid = $window.localStorage.getItem('idUsuarios');
            if (userid){
                $scope.dado = UserServices.get({ id: userid });
            }            
        };
        
       
      
        //Salva e Atualiza
        
        $scope.submitForm = function(isValid) {

            if (isValid) {
              
                var saida = $scope.dado;
                var id = $window.localStorage.getItem('idUsuarios');
                if(id && saida){
                    
                    UserGet.update(saida, function(){                        
                        $state.go("usuarios");
                    });
                    alert("Atualiza Usuario");
                }
                else { 
                    UserGet.save(saida, function(){
                        $state.go("usuarios");
                    });
                    alert("Novo Usuario");
                   
                }
            }

        };
          
        // Deleta registro
        $scope.deletar = function(username, userid) {
            if (confirm("Deseja excluir " + username + "?")) {
               
                UserServices.delete({ id: userid },function(){                        
                        $state.reload();
                });
            }
           
        };
    }]);
